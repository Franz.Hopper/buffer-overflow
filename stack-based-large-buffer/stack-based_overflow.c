#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void say_hello(char *nom){
  char buffer[100];

  // Copie de l'argument dans le buffer avec possible dépassement sur
  // les sauvegardes de ebp et eip
  strcpy(buffer, nom);

  printf("Bonjour %s !\n", buffer);

  // Fin de la fonction : c'est ici que eip est remplacée par l'adresse
  // de retour située dans la pile
}
 
int main(int argc, char *argv[]){
  if(argc != 2){
    printf("Usage : %s <name>\n", argv[0]);
    exit(0);
  }

  say_hello(argv[1]);

  printf("Tout s'est déroulé normalement\n");

  return 0;
}